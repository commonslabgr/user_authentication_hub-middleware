import socketserver
import sys
import json
from http.server import BaseHTTPRequestHandler
import uah_controller
import ssl
sys.path.append('ui')
import handler

class httpsHandler(BaseHTTPRequestHandler):
    def _set_json_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _set_html_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def _set_image_headers(self):    
        self.send_response(200)
        self.send_header('Content-type', 'image/png')
        self.end_headers()

    def do_GET(self):
        if self.path == '/auth':
            self._set_json_headers()
            data = uah_controller.oidc_getAccessToken()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/token':
            self._set_json_headers()
            data = uah_controller.oidc_getIDToken()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/userinfo':
            self._set_json_headers()
            data = uah_controller.oidc_getUserInfo()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/logout':        
            self._set_html_headers()
            data = uah_controller.oidc_Logout()
            self.wfile.write(b'Logout')
        elif self.path.__contains__('.png'):
            imgname = self.path
            print ("Image requested is: ", imgname[1:])
            imgfile = open(imgname[1:], 'rb').read()
            self._set_image_headers()
            self.wfile.write(imgfile)
        else:
            self._set_html_headers()
            #Show home page with choices for demo
            f = open('ui/home.html', 'r')
            home_contents = f.read()
            self.wfile.write(home_contents.encode('utf-8'))

socketserver.TCPServer.allow_reuse_address = True
httpd = socketserver.TCPServer(("", 4443), httpsHandler )
httpd.socket = ssl.wrap_socket (httpd.socket,
                                keyfile="certs/keycloak.key",
                                certfile="certs/keycloak.pem",
                                server_side=True)

handler.ShowRunning()
httpd.serve_forever()