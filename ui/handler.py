import os
from bs4 import BeautifulSoup

filename = '/home/pi/user_authentication_hub-middleware/ui/index.html'
soup = BeautifulSoup(open(filename), 'html5lib')

#Start chromium
#DISPLAY=:0 chromium-browser --kiosk file:///home/pi/user_authentication_hub-middleware/ui/index.html
#chromium-browser 

def saveHTML(html):
    html.prettify(formatter="html5")
    with open(filename, "w", encoding='utf-8') as file:
        file.write(str(html))

#Refresh chromium
def RefreshBrowser():
    cmd = "DISPLAY=:0 xdotool key F5"
    os.system(cmd)

def ShowRunning():
    soup.find("div", {"id": "message"}).string.replace_with('Service is running')
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/empty.png'
    soup.find("div", {"id": "footer"}).string.replace_with('UAH module is running...')
    saveHTML(soup)
    RefreshBrowser()

def ShowError(error):
    return 0


def setMessage(message):
    #Update message
    soup.find("div", {"id": "message"}).string.replace_with(message)
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/success.png'
    saveHTML(soup)
    RefreshBrowser()


def msgPleaseSwypeCard():
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/empty.png'    
    soup.find("div", {"id": "message"}).string.replace_with('Please place your card close to the reader...')
    saveHTML(soup)
    RefreshBrowser()


def AuthenticationSuccess(username):
    #Update message
    soup.find("div", {"id": "message"}).string.replace_with(username+' succesfully authenticated')
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/success.png'
    saveHTML(soup)
    RefreshBrowser()

def Logout():
    #Update message
    soup.find("div", {"id": "message"}).string.replace_with('User has been logged out')
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/success.png'
    saveHTML(soup)
    RefreshBrowser()

def AuthenticationFail(error):
    #Update message
    soup.find("div", {"id": "message"}).string.replace_with('Authentication failed: '+error)
    #Update status icon
    soup.find("img", {"id": "imgstatus"})['src'] = 'assets/fail.png'
    saveHTML(soup)
    RefreshBrowser()
    