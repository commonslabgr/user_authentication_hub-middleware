import os
import sys
import time
import subprocess
import logging
import sys
import keycloak_client
sys.path.append('ui')
import handler
sys.path.append('cardReader')
import cardreader


#Read NFC card and get ID
def ReadCard():
    #line = input("Username: ")
    #line = line+" "+input("Password: ")
    line = cardreader.read()
    line = line.strip()
    return line

#Get Token ID from keycloak
def oidc_getIDToken():
    #Call UI module to ask user to authenticate
    handler.msgPleaseSwypeCard()
    #ReadCard
    line = ReadCard()
    try:
        username, password = line.split(' ')
    except ValueError:
        handler.AuthenticationFail('Invalid card')
        return 'Error'
    else:
        token = keycloak_client.getToken(username, password)
        handler.AuthenticationSuccess(username)
        return token

#Get Access Token from keycloak
def oidc_getAccessToken():
    #Call UI module to ask user to authenticate
    handler.msgPleaseSwypeCard()
    #ReadCard
    line = ReadCard()
    try:
        username, password = line.split(' ')
    except ValueError:
        handler.AuthenticationFail('Invalid card')
        return 'Error'
    else:
        token = keycloak_client.getToken(username, password)
        handler.AuthenticationSuccess(username)
        return token['access_token']


#Get Token ID from keycloak
def oidc_getUserInfo():
    #Call UI module to ask user to authenticate
    handler.msgPleaseSwypeCard()
    #ReadCard
    line = ReadCard()
    try:
        username, password = line.split(' ')
    except ValueError:
        handler.AuthenticationFail('Invalid card')
        return 'Error'
    else:
        token = keycloak_client.getToken(username, password)
        token = keycloak_client.getUserInfo(token['access_token'])
        handler.setMessage('Name:'+token['given_name']+' Email:'+token['email'])
        return token
    

#Get Token ID from keycloak
def oidc_Logout():
    #Call UI module to ask user to authenticate
    handler.msgPleaseSwypeCard()
    #ReadCard
    line = ReadCard()
    try:
        username, password = line.split(' ')
    except ValueError:
        handler.AuthenticationFail('Invalid card')
        return 'Error'
    else:
        token = keycloak_client.getToken(username, password)
        handler.Logout()    
        return 'Logged out'

def main():
    handler.ShowRunning()

if __name__ == "__main__":
    main()