#!/bin/bash
sleep 15
export XAUTHORITY=/home/pi/.Xauthority; export DISPLAY=:0; xdotool mousemove 900 600
while :
do
    exec pkill feh &
    sleep 0.1
    exec /home/pi/user_authentication_hub-middleware/cardReader/demo/showstandby.sh &
    card=$('/home/pi/user_authentication_hub-middleware/cardReader/demo/read.py')
    if [[ "$card" == *"OK"* ]] ; then
        echo "Authenticated"
        exec pkill feh &
        echo "Killed feh"
        sleep 0.1
        exec /home/pi/user_authentication_hub-middleware/cardReader/demo/showauthenticated.sh &
        echo "Started feh"
    else
        echo "NOT Authenticated"
        exec pkill feh &
        sleep 0.1
        exec /home/pi/user_authentication_hub-middleware/cardReader/demo/shownotauthenticated.sh &
    fi
    sleep 3
    #exec pkill feh &
done
