import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522

reader = SimpleMFRC522()
try:
        text = input("Type new data you want written to the card: ")
        print("Now place your tag to write")
        reader.write(text)
        print("Check screen for messages, if no messages, write was successful")
finally:
        GPIO.cleanup()