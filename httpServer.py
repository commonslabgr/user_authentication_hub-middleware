import socketserver
import sys
import json
from http.server import BaseHTTPRequestHandler
import uah_controller

def get_oidc_token():
    token = uah_controller.oidc_getTokenID()
    return token

class httpHandler(BaseHTTPRequestHandler):
    def _set_json_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _set_html_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def _set_image_headers(self):    
        self.send_response(200)
        self.send_header('Content-type', 'image/png')
        self.end_headers()

    def do_GET(self):
        if self.path == '/auth':
            self._set_json_headers()
            data = uah_controller.oidc_getAccessToken()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/token':
            self._set_json_headers()
            data = uah_controller.oidc_getIDToken()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/userinfo':
            self._set_json_headers()
            data = uah_controller.oidc_getUserInfo()
            print (data)
            self.wfile.write(json.dumps(data).encode('utf-8'))
        elif self.path == '/logout':        
            self._set_html_headers()
            data = uah_controller.oidc_Logout()
            print ('Logout')
        elif self.path.__contains__('.png'):
            imgname = self.path
            print ("Image requested is: ", imgname[1:])
            imgfile = open(imgname[1:], 'rb').read()
            self._set_image_headers()
            self.wfile.write(imgfile)
        else:
            self._set_html_headers()
            #Show home page with choices for demo
            #Get access token
            #Get Token ID
            #Get user info
            #Logout
            f = open('ui/home.html', 'r')
            home_contents = f.read()
            self.wfile.write(home_contents.encode('utf-8'))

socketserver.TCPServer.allow_reuse_address = True
httpd = socketserver.TCPServer(("", 8088), httpHandler)
httpd.serve_forever()