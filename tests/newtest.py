import requests
import logging
import json
import ssl
import xmlrpc

ssl.match_hostname = lambda cert, hostname: True

def login():
    idp_access_token = idp_login()
    return keycloak_token_exchange(idp_access_token)

def idp_login():
    login_data = {
        "client_id": "raspberrypi",
        "client_secret":"31e1e909-e37a-48d5-8c76-74d7ca8e3b60",
        "grant_type": "password",
        "username": "dkoukoul",
        "password": "password",
        "scope": "openid",
        "realm": "UAH"
    }
    login_headers = {
        "Content-Type": "application/json"
    }
    #token_response = requests.post(<IDP-URL>, headers=login_headers, data=json.dumps(login_data))
    token_response = requests.post('https://uah.local:8443/auth/realms/UAH/protocol/openid-connect/auth', verify=False, headers=login_headers, data=json.dumps(login_data))
    #json_data = json.loads(token_response.text)
    #string = response.read().decode('utf-8')
    #data = json.loads(token_response.json())
    print (token_response.json())
    #print (token_response.json())
    #return parse_response(token_response)['access_token']
    #return token_response.json()['access_token']
    return ''

def keycloak_token_exchange(idp_access_token):
    token_exchange_url = 'https://uah.local:8443/auth/realms/UAH/protocol/openid-connect/token'
    data = {
        'grant_type': 'urn:ietf:params:oauth:grant-type:token-exchange',
        'subject_token': idp_access_token,
        'subject_issuer': 'https://uah.local:8443/auth/realms/UAH',
        'subject_token_type': 'urn:ietf:params:oauth:token-type:access_token',
        'audience': 'raspberrypi'
    }
    response = requests.post(token_exchange_url, data=data, verify=False,
                             auth=('raspberrypi', '31e1e909-e37a-48d5-8c76-74d7ca8e3b60'))
    print (response.text)
    return ''
    #return parse_response(response)['access_token']
    #return json.loads(response.text)['access_token']

if __name__ == '__main__':
    login()    