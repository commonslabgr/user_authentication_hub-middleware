from oic import rndstr
from oic.utils.http_util import Redirect

session["state"] = rndstr()
session["nonce"] = rndstr()
args = {
    "client_id": client.client_id,
    "response_type": "code",
    "scope": ["openid"],
    "nonce": session["nonce"],
    "redirect_uri": client.registration_response["redirect_uris"][0],
    "state": session["state"]
}

auth_req = client.construct_AuthorizationRequest(request_args=args)
login_url = auth_req.request(client.authorization_endpoint)

return Redirect(login_url)