########## initialize connection ###############################################

import ldap
con = ldap.initialize('ldap://192.168.1.14:10389')

# At this point, we're connected as an anonymous user
# If we want to be associated to an account
# you can log by binding your account details to your connection

#con.simple_bind_s("cn=testuser,ou=People,dc=activage-isere,dc=eu", "password")

########## performing a simple ldap query ####################################

ldap_base = "dc=activage-isere,dc=eu"
query = "(uid=dkoukoul)"
result = con.search_s(ldap_base, ldap.SCOPE_SUBTREE, query)