import ssl
from keycloak import KeycloakOpenID

ssl.match_hostname = lambda cert, hostname: True

# Configure client
#KeycloakOpenID(server_url="https://uah.local:8443/auth/realms/UAH/protocol/openid-connect/auth",
keycloak_openid = KeycloakOpenID(server_url="https://uah.local:8443/auth/",
                    client_id="raspberrypi",
                    realm_name="UAH",
                    client_secret_key="31e1e909-e37a-48d5-8c76-74d7ca8e3b60")

# Get WellKnow
#config_well_know = keycloak_openid.well_know()
#print (config_well_know)
# Get Token
token = keycloak_openid.token("dkoukoul", "password")
print (token)
#token = keycloak_openid.token("user", "password", totp="012345")

# Get Userinfo
userinfo = keycloak_openid.userinfo(token['access_token'])

# Refresh token
token = keycloak_openid.refresh_token(token['refresh_token'])

# Logout
keycloak_openid.logout(token['refresh_token'])

# Get Certs
#certs = keycloak_openid.certs()

# Get RPT (Entitlement)
#token = keycloak_openid.token("user", "password")
#rpt = keycloak_openid.entitlement(token['access_token'], "resource_id")

# Instropect RPT
#token_rpt_info = keycloak_openid.introspect(keycloak_openid.introspect(token['access_token'], rpt=rpt['rpt'], token_type_hint="requesting_party_token"))

# Introspect Token
#token_info = keycloak_openid.introspect(token['access_token'])

# Decode Token
#KEYCLOAK_PUBLIC_KEY = "secret"
#options = {"verify_signature": True, "verify_aud": True, "exp": True}
#token_info = keycloak_openid.decode_token(token['access_token'], key=KEYCLOAK_PUBLIC_KEY, options=options)

# Get permissions by token
#token = keycloak_openid.token("user", "password")
#keycloak_openid.load_authorization_config("example-authz-config.json")
#policies = keycloak_openid.get_policies(token['access_token'], method_token_info='decode', key=KEYCLOAK_PUBLIC_KEY)
#permissions = keycloak_openid.get_permissions(token['access_token'], method_token_info='introspect')
