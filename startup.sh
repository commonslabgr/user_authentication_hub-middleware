#!/bin/bash

echo "Launching Keycloak Server..."
/home/pi/keycloak-6.0.1/bin/standalone.sh &
sleep 20

#Start HTTPS server
python3 /home/pi/user_authentication_hub-middleware/httpsServer.py &

#Move mouse out of view
DISPLAY=:0 xdotool mousemove 0 0

echo "Launching Chromium..."
DISPLAY=:0 chromium-browser --kiosk file:///home/pi/user_authentication_hub-middleware/ui/index.html

